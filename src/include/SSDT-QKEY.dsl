// for laptops that need 102-key layout

//DefinitionBlock("", "SSDT", 2, "hack", "key102", 0)
//{
    External(\_SB.PCI0, DeviceObj)
    External(\_SB.PCI0.LPCB, DeviceObj)

    External (_SB.PCI0.LPCB.EC, DeviceObj)
    Scope (_SB.PCI0.LPCB.EC)
    {
        Method (_Q11, 0, NotSerialized)  // _Qxx: EC Query, xx=0x00-0xFF
        {
            Notify (\_SB.PCI0.LPCB.PS2K, 0x0405) // brightness Down
        }

        Method (_Q12, 0, NotSerialized)  // _Qxx: EC Query, xx=0x00-0xFF
        {
            Notify (\_SB.PCI0.LPCB.PS2K, 0x0406)  // brightness Up
        }
        //Method (_Q13, 0, NotSerialized)  // _Qxx: EC Query, xx=0x00-0xFF
        //{
            //Notify (\_SB.PCI0.LPCB.PS2K, 0x046e)  // Mirror toggle
        //}
    }
//}
