    // In DSDT, native _PTS and _WAK are renamed ZPTS/ZWAK
    // As a result, calls to these methods land here.
    Method(_PTS, 1)
    {
        // Disable XHC PMEE if XPEE is specified 1
        // This fixes "auto restart after shutdown" with USB devices connected
        If (1 == \RMCF.XPEE && 5 == Arg0)
        {
            External(\_SB.PCI0.XHC.PMEE, FieldUnitObj)
            If (CondRefOf(\_SB.PCI0.XHC.PMEE)) { \_SB.PCI0.XHC.PMEE = 0 }
        }
        If (\RMCF.SHUT && 5 == Arg0) { Return }
        If (\RMCF.DGPU & 2) { \RMCF.RMON() }
        External(\ZPTS, MethodObj)
        ZPTS(Arg0)
    }
    Method(_WAK, 1)
    {
        If (Arg0 < 1 || Arg0 > 5) { Arg0 = 3 }
        External(\ZWAK, MethodObj)
        Local0 = ZWAK(Arg0)
        If (\RMCF.DGPU & 2) { \RMCF.RMOF() }
        Return(Local0)
    }
    Device(RMD1)
    {
        Name(_HID, "RMD10000")
        Method(_INI)
        {
            If (\RMCF.DGPU & 1) { \RMCF.RMOF() }
        }
    }

    External(_SB.PCI0.LPCB.EC, DeviceObj)
    External(_SB.PCI0.LPCB.EC.XREG, MethodObj)

    // original _REG is renamed to XREG
    Scope(_SB.PCI0.LPCB.EC)
    {
        OperationRegion(ECR3, EmbeddedControl, 0x00, 0xFF)
        Method(_REG, 2)
        {
            // call original _REG (now renamed XREG)
            XREG(Arg0, Arg1)

            // call RDSS(0) for _OFF/HGOF
            If (3 == Arg0 && 1 == Arg1 && \RMCF.DGPU & 1) { \RMCF.RDSS(0) }
        }
    }
//EOF