// Standard PS2 overrides
// Use Include directive from model specific SSDT

    External(\_SB.PCI0.LPCB.PS2K, DeviceObj)
    Scope (\_SB.PCI0.LPCB.PS2K)
    {
        // overrides for VoodooPS2 configuration...
        Name(RMCF, Package()
        {
            "Sentelic FSP",
            Package (0x02)
            {
                "DisableDevice",
                ">y"
            },

            "ALPS GlidePoint",
            Package (0x02)
            {
                "DisableDevice",
                ">y"
            },

            "Mouse",
            Package (0x02)
            {
                "DisableDevice",
                ">y"
            },

            "Keyboard",
            Package (0x04)
            {
                "Custom PS2 Map",
                Package (0x05)
                {
                    Package (0x00){},
                    "e0ab=0",         // bogus Fn+F2/F3
                    "e005=0",         // ???
                    //"e037=64",        // PrtScr=F13
                    //"e045=68",        // PauseBreak=F18
                },

                "Custom ADB Map",
                Package (0x04)
                {
                    Package (0x00){},
                    "e019=42",         // next
                    "e010=40",          // previous
                    //"e06a=70",          // ???
                }
            },

            "Synaptics TouchPad",
            Package (0x08)
            {
                "DynamicEWMode", ">y",
                "MultiFingerVerticalDivisor", 9,
                "MultiFingerHorizontalDivisor", 9,
                "MomentumScrollThreshY", 12
            }
        })
    }

//EOF
