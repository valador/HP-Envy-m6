#define LAYOUTID 13

// inject properties for audio

    External(_SB.PCI0.HDEF, DeviceObj)
    Method(_SB.PCI0.HDEF._DSM, 4)
    {
        If (!Arg2) { Return (Buffer() { 0x03 } ) }
        Return(Package()
        {
            "layout-id", Buffer(4) { LAYOUTID, 0, 0, 0 },
            "No-hda-gfx", Buffer(8) { 0, 0, 0, 0, 0, 0, 0, 0},
            "PinConfigurations", Buffer() { },
            "AAPL,slot-name", Buffer() { "Internal@0,1,0" },
            "device_type", Buffer() { "Audio device" },
            "model", Buffer() { "Intel HDA - IDT92HD91BXX (76e0)" },
        })
    }

// CodecCommander configuration

    Name(_SB.PCI0.HDEF.RMCF, Package()
    {
        "CodecCommander", Package() { "Disable", ">y", },
        "CodecCommanderPowerHook", Package() { "Disable", ">y", },
    })

//EOF
