// DEPRECATED, Mojave not have AirPortAtheros40.kext
#ifndef NO_DEFINITIONBLOCK
DefinitionBlock ("", "SSDT", 1, "toleda", "ami8arpt", 0)
{   
#endif
    External (_SB_.PCI0.RP02, DeviceObj)
    External (_SB_.PCI0.RP02.PXSX, DeviceObj)
    External (PXSX, DeviceObj)

    Scope (\_SB.PCI0.RP02)
    {
        Scope (PXSX)
        {
            Name (_STA, Zero)  // _STA: Status
        }

        Device (ARPT)
        {
            Name (_ADR, Zero)  // _ADR: Address
            Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
            {
                0x09, 
                0x04
            })
            Method (_DSM, 4, NotSerialized)  // _DSM: Device-Specific Method
            {
                If ((Arg2 == Zero))
                {
                    Return (Buffer (One)
                    {
                         0x03                                             // .
                    })
                }

                Return (Package ()
                {
                    "AAPL,slot-name", 
                    "AirPort", 
                    "built-in", 
                    Buffer (One)
                    {
                        0x00                                           
                    }, 

                    "device_type", 
                    "AirPort", 
                    "model", 
                    "Atheros AR9x8x 802.11 a/b/g/n Wireless Network Controller", 
                    "name", 
                    "AirPort Extreme", 
                    "compatible", 
                    "pci168c,30"
                })
            }
        }
    }
#ifndef NO_DEFINITIONBLOCK
}
#endif
//EOF