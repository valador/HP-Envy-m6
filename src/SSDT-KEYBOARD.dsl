DefinitionBlock("", "SSDT", 2, "hack", "keyb", 0)
{
    External(\_SB.PCI0.LPCB.PS2K, DeviceObj)
    Scope (\_SB.PCI0.LPCB.PS2K)
    {
        // Select specific keyboard map in VoodooPS2Keyboard.kext
        Method(_DSM, 4)
        {
            If (!Arg2) { Return (Buffer() { 0x03 } ) }
            Return (Package()
            {
                "RM,oem-id", "HPQOEM",
                "RM,oem-table-id", "Haswell-Envy-RMCF",
            })
        }

        Name(RMCF, Package()
        {
            "Sentelic FSP",
            Package (0x02)
            {
                "DisableDevice",
                ">y"
            },

            "ALPS GlidePoint",
            Package (0x02)
            {
                "DisableDevice",
                ">y"
            },

            "Mouse",
            Package (0x02)
            {
                "DisableDevice",
                ">y"
            },

            "Keyboard",
            Package (0x04)
            {
                "Custom PS2 Map",
                Package (0x05)
                {
                    Package (0x00){},
                    "e0ab=0",         // bogus Fn+F2/F3
                    "e005=0",         // ???
                    //"e037=64",        // PrtScr=F13
                    //"e045=68",        // PauseBreak=F18
                },

                "Custom ADB Map",
                Package (0x04)
                {
                    Package (0x00){},
                    "e019=42",         // next
                    "e010=40",          // previous
                    //"e06a=70",          // ???
                }
            },

            "Synaptics TouchPad",
            Package (0x08)
            {
                "DynamicEWMode", ">y",
                "MultiFingerVerticalDivisor", 9,
                "MultiFingerHorizontalDivisor", 9,
                "MomentumScrollThreshY", 12
            }
        })
    }
    External(_SB.PCI0.LPCB.EC, DeviceObj)
    Scope(_SB.PCI0.LPCB.EC)
    {
        // The native _Qxx methods in DSDT are renamed XQxx,
        // so notifications from the EC driver will land here.

        // _Q10/Q11 called on brightness down/up
        Method (_Q11, 0, NotSerialized)
        {
            // Brightness Down
            Notify(\_SB.PCI0.LPCB.PS2K, 0x0405)
        }
        Method (_Q12, 0, NotSerialized)
        {
            // Brightness Up
            Notify(\_SB.PCI0.LPCB.PS2K, 0x0406)
        }
    }
}

//EOF
