// SSDT for 4x0 G1 Ivy

DefinitionBlock("", "SSDT", 2, "hack", "envym6", 0)
{
    #define NO_DEFINITIONBLOCK
    #include "include/SSDT-RMCF.dsl"
    #include "include/SSDT-XOSI.dsl"
    #include "include/SSDT-XPRW.dsl"
    //#include "include/SSDT-WIFI.dsl"
    #include "include/SSDT-PEGP_DGFX_RDSS.dsl" //- for external graphics
    #include "include/SSDT-DGPU.dsl"
    //#include "include/IDT_76e0_HDEF.dsl"
    // #include "include/standard_PS2K.dsl"
    // #include "include/SSDT-KEY102.dsl"
    // #include "include/SSDT-QKEY.dsl"
    //#include "include/SSDT-XHC.dsl"
    //#include "include/SSDT-BATT.dsl"
    #include "include/SSDT-EH01.dsl" //REVIEW: placing at end as no USB customization data available
    #include "include/SSDT-EH02.dsl" //REVIEW: placing at end as no USB customization data available
    //#include "include/SSDT-USB.dsl"
}
//EOF
