# makefile

#
# Patches/Installs/Builds DSDT patches for Intel NUC (5/6/7/8), Intel Compute Stick (6)
#
# Created by RehabMan 
#

# set build products
BUILDDIR=./build
SRCPATCH=./src
AML_PRODUCTS=$(BUILDDIR)/SSDT-EnvyM6.aml \
	$(BUILDDIR)/SSDT-IGPU.aml \
	$(BUILDDIR)/SSDT-CPU.aml \
	$(BUILDDIR)/SSDT-SATA.aml \
	$(BUILDDIR)/SSDT-KEYBOARD.aml \
	$(BUILDDIR)/SSDT-USB.aml \
	$(BUILDDIR)/SSDT-WIFI.aml \
	$(BUILDDIR)/SSDT-FANREAD.aml $(BUILDDIR)/SSDT-FANORIG.aml $(BUILDDIR)/SSDT-FANQ.aml $(BUILDDIR)/SSDT-FANRM.aml $(BUILDDIR)/SSDT-FANGRAP.aml
PRODUCTS=$(AML_PRODUCTS)

IASLOPTS=-vw 2095 -vw 2146 -vw 2089 -vw 4089 -vi -vr
IASL=iasl

.PHONY: all
all: $(PRODUCTS)

$(BUILDDIR)/%.aml : $(SRCPATCH)/%.dsl
	iasl $(IASLOPTS) -p $@ $<

$(BUILDDIR)/SSDT-IGPU.aml : $(SRCPATCH)/SSDT-IGPU.dsl
# generated with: ./find_dependencies.sh

$(BUILDDIR)/SSDT-EnvyM6.aml : $(SRCPATCH)/SSDT-EnvyM6.dsl

$(BUILDDIR)/SSDT-USB.aml : $(SRCPATCH)/SSDT-USB.dsl

$(BUILDDIR)/SSDT-SATA.aml : $(SRCPATCH)/SSDT-SATA.dsl

$(BUILDDIR)/SSDT-KEYBOARD.aml : $(SRCPATCH)/SSDT-KEYBOARD.dsl

$(BUILDDIR)/SSDT-WIFI.aml : $(SRCPATCH)/SSDT-WIFI.dsl

# $(BUILDDIR)/SSDT-XOSI.aml : $(SRCPATCH)/SSDT-XOSI.dsl

#$(BUILDDIR)/SSDT-XPRW.aml : $(SRCPATCH)/SSDT-XPRW.dsl

$(BUILDDIR)/SSDT-FANQ.aml : $(SRCPATCH)/SSDT-FANQ.dsl
	iasl -D QUIET $(IASLOPTS) -p $@ $<

$(BUILDDIR)/SSDT-FANRM.aml : $(SRCPATCH)/SSDT-FANQ.dsl
	iasl -D REHABMAN $(IASLOPTS) -p $@ $<

$(BUILDDIR)/SSDT-FANGRAP.aml : $(SRCPATCH)/SSDT-FANQ.dsl
	iasl -D GRAPPLER $(IASLOPTS) -p $@ $<
# end generated

.PHONY: clean
clean:
	rm -f $(BUILDDIR)/*.dsl $(BUILDDIR)/*.aml

# NUC5
.PHONY: install_nuc5
install_nuc5: $(AML_PRODUCTS)
	$(eval EFIDIR:=$(shell ./mount_efi.sh))
	rm -f "$(EFIDIR)"/EFI/CLOVER/ACPI/patched/SSDT-*.aml
	rm -f "$(EFIDIR)"/EFI/CLOVER/ACPI/patched/SSDT_*SPF.aml "$(EFIDIR)"/EFI/CLOVER/ACPI/patched/SSDT_CFLALT.aml
	rm -f "$(EFIDIR)"/EFI/CLOVER/ACPI/patched/SSDT.aml
	cp $(BUILDDIR)/SSDT-NUC5.aml "$(EFIDIR)"/EFI/CLOVER/ACPI/patched
	cp $(BUILDDIR)/SSDT-DDA.aml "$(EFIDIR)"/EFI/CLOVER/ACPI/patched
